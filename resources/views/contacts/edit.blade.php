@extends('contacts.layout')

@section('content')
        <div class="card uper">
            <div class="card-header">
                Edit Contact
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <edit-contact datafromcontroller='{{ json_encode($contact) }}'></edit-contact>
            </div>
        </div>
@endsection
