<!DOCTYPE html>
<html>
<head>
    <title>Laravel Test loc Application</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
    <body>
        <div class="container" id="app">
            <notification></notification>
            @yield('content')
        </div>
        <script src="/js/app.js"></script>
    </body>
</html>
