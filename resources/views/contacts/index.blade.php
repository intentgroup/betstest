@extends('contacts.layout')
@section('content')
    <div class="container">
        <div class="uper">
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div><br />
            @endif
            <a href="{{ route('contacts.create')}}" class="btn btn-primary">Create</a>

            <index-contact datafromcontroller='{{ json_encode($contacts) }}'></index-contact>
        </div>
    </div>

@endsection
