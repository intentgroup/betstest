@extends('contacts.layout')

@section('content')
        <div class="card uper">
            <div class="card-header">
                Contact Information
            </div>
            <div class="card-body">
                <show-contact datafromcontroller='{{ json_encode($contact) }}'></show-contact>
            </div>
        </div>
@endsection
