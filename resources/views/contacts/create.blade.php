@extends('contacts.layout')
@section('content')

    <div class="card uper">
        <div class="card-header">
            Add Contact
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <create-contact></create-contact>

        </div>
    </div>
@endsection
