<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Contact extends Model
{
    protected $appends = ['age'];

    protected $fillable = [
        'name',
        'email',
        'company',
        'title',
        'birthdate',
        'is_notify',
        'notification_time',
    ];

    protected $casts = [
        'notification_time' => 'array'
    ];

    /**
     * Get the age from birth date
     *
     * @param  null
     * @return int
     */
    public function getAgeAttribute()
    {
        $years = Carbon::parse($this->attributes['birthdate'])->age;
        return $years;
    }

    /**
     * Get the interval from notification_time array
     *
     * @param  null
     * @return int
     */
    public function getNotificationIntervalAttribute()
    {
        $interval = null;
        $notification_time = json_decode($this->attributes['notification_time']);

        if(count($notification_time) == 2) {
            $interval_arr = reset($notification_time);
            $interval = reset($interval_arr);
        }

        return $interval;
    }

    /**
     * Get the type from notification_time array
     *
     * @param  null
     * @return int
     */
    public function getNotificationTypeAttribute()
    {
        $type = null;
        $notification_time = json_decode($this->attributes['notification_time']);

        if(count($notification_time) == 2) {
            $type_arr = next($notification_time);
            $type = reset($type_arr);

        }
        return $type;
    }

    /**
     * Get the interval in seconds from mixed minutes/seconds value
     *
     * @param  null
     * @return int
     */
    public function getNotificationIntervalSecondsAttribute()
    {
        $seconds = 0;
        $notification_time = json_decode($this->attributes['notification_time']);

        if(count($notification_time) == 2) {
            $interval_arr = reset($notification_time);
            $interval = reset($interval_arr);
            $type_arr = next($notification_time);
            $type = reset($type_arr);
            if($type == 'minutes') {
                $seconds = $interval*60; // 60 seconds in 1 minutes
            } else {
                $seconds = $interval;
            }
        }

        return $seconds;
    }
}
