<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::all();

        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|max:255',
            'email'=>'required|email:rfc,dns|unique:contacts',
            'company'=>'required|max:255',
            'title'=>'required|max:255',
            'birthdate'=> 'required|date',
        ]);


        $contact = new Contact([
            'name' => $request->get('name'),
            'email'=> $request->get('email'),
            'company'=> $request->get('company'),
            'title'=> $request->get('title'),
            'birthdate'=> $request->get('birthdate'),
            'is_notify'=> (int) $request->get('is_notify',0),
            'notification_time'=> $request->get('notification_time'),

        ]);
        $contact->save();
        //return redirect('/contacts')->with('success', 'Contact has been added');
        return response()->json("successfully added");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::find($id);

        return view('contacts.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::find($id);

        return view('contacts.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|max:255',
            'email'=>"required|email:rfc,dns|unique:contacts,email,{$id}",
            'company'=>'required|max:255',
            'title'=>'required|max:255',
            'birthdate'=> 'required|date',
        ]);

        $contact = Contact::find($id);
        $contact->name = $request->get('name');
        $contact->email = $request->get('email');
        $contact->company = $request->get('company');
        $contact->title = $request->get('title');
        $contact->birthdate = $request->get('birthdate');
        $contact->is_notify = (int) $request->get('is_notify', 0);
        $contact->notification_time = $request->get('notification_time');
        $contact->save();

        return response()->json('successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();

        $contacts = Contact::all();
        return response()->json($contacts);
    }

}
