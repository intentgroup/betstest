<?php

namespace App\Console\Commands;

use App\Jobs\ProcessNotification;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Contact;

class SendNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:send {stop=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notifications to the contacts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start Process Notification');
        $stop = $this->argument('stop');
        while(true) {
            if($stop == 'true') {
                $this->info('Stop Notification');
                break;
            }
            $now_date = Carbon::now()->format('Y-m-d H:i:s');
            $contacts = Contact::where('is_notify', 1)
                ->where('notify_at', '<', $now_date)
                ->get();
            foreach($contacts as $contact) {
                $this->info('Send Notification to ' . $contact->name);
                dispatch(new ProcessNotification($contact));
                $contact->notify_at = Carbon::now()->addSeconds($contact->notification_interval_seconds)->format('Y-m-d H:i:s');
                $contact->save();
            }
            sleep(1);
        }
    }
}
