<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'company' => Str::random(10),
            'title' => Str::random(10),
            'birthdate' => Carbon::create('1998', '03', '01'),
            'is_notify' => (bool)random_int(0, 1),
            'notification_time' => json_encode(array(array('interval' => random_int(1, 60)), array('type' => rand(0, 1) ? 'seconds' : 'minutes'))),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
